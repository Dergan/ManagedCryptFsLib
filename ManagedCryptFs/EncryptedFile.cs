﻿using ManagedCryptFs.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ManagedCryptFs
{
    public class EncryptedFile : IDisposable
    {
        public const int FILE_HEADER_SIZE = 1024 * 128; //a fixed header size (128KB) mostly reserved space

        public const int BLOB_SIZE = PAYLOAD_BLOB_SIZE + BLOB_HEADER_SIZE; //36KB blob size, blob header has about ~6KB header reserved space
        public const int BLOB_HEADER_SIZE = 1024 * 6; //6KB blob header size

        public const int MAX_PAYLOAD_BLOB_SIZE = 1024 * 30; //30KB max payload size
        public const int PAYLOAD_BLOB_SIZE = 1024 * 35; //35KB payload size, ~5KB reserved space

        public string TargetFilePath { get; set; }
        public FileHeader Header { get; private set; }
        public Stream Stream { get; private set; }
        public PrivateKeyFile keyFile { get; private set; }
        public bool IsDisposed { get; private set; }
        
        public long BlobCount
        {
            get
            {
                long size = Stream.Length - FILE_HEADER_SIZE;
                return size / BLOB_SIZE;
            }
        }
        
        public EncryptedFile()
        {

        }

        public static EncryptedFile OpenFile(Stream FromFs, PrivateKeyFile keyFile, long Position)
        {
            EncryptedFile encryptedFile = new EncryptedFile();
            encryptedFile.Stream = FromFs;
            encryptedFile.keyFile = keyFile;

            encryptedFile.Header = new FileHeader();
            encryptedFile.Header.File = encryptedFile;
            encryptedFile.Header.DecryptHeader(FromFs, Position, keyFile);

            return encryptedFile;
        }

        internal long GetIndexByPosition(long Position)
        {
            return Position / EncryptedFile.MAX_PAYLOAD_BLOB_SIZE;
        }

        public EncryptedBlob GetBlobByPosition(long Position)
        {
            return GetBlobByIndex(GetIndexByPosition(Position));
        }

        public EncryptedBlob GetBlobByIndex(long Index)
        {
            lock (Stream)
            {
                if (Index >= BlobCount)
                    return null;

                Stream.Position = FILE_HEADER_SIZE + (Index * BLOB_SIZE);
                EncryptedBlob blob = new EncryptedBlob();
                blob.File = this;
                blob.Header = this.Header;
                blob.DecryptBlobHeader(Stream, Stream.Position);
                return blob;
            }
        }

        public static EncryptedFile EncryptFile(Stream FromFs, Stream DestFs, string FileName, PrivateKeyFile keyFile)
        {
            SecureRandom rand = new SecureRandom();

            EncryptedFile file = new EncryptedFile();
            file.Stream = DestFs;

            file.Header = new FileHeader();
            file.Header.Name = FileName;
            file.Header.RandomSeed = rand.Next();
            //header.EncryptionTypes.Add(EncryptionType.AES_128);
            file.Header.EncryptionTypes.Add(EncryptionType.AES_256);
            file.Header.Salt = rand.NextBytes(32);

            //create for every encryption type a random key
            for (int i = 0; i < file.Header.EncryptionTypes.Count; i++)
                file.Header.MasterKeys.Add(rand.NextBytes(32));

            if (file.Header.MasterKeys.Count == 0)
                throw new Exception("No encryption used!");

            file.Header.EncryptHeader(DestFs, 0, keyFile);

            EncryptedFileStream stream = new EncryptedFileStream(file);

            //encrypt the file from FromFs -> DestFs
            for (long i = 0; i < FromFs.Length; i += MAX_PAYLOAD_BLOB_SIZE)
            {
                long ReadLen = i + MAX_PAYLOAD_BLOB_SIZE < FromFs.Length ? MAX_PAYLOAD_BLOB_SIZE : FromFs.Length - FromFs.Position;

                byte[] payload = new byte[ReadLen];
                int read = FromFs.Read(payload, 0, payload.Length);

                if (read <= 0)
                    break;
                
                stream.Write(payload, 0, read);
            }
            return file;
        }

        internal byte[] ReadData(Stream stream, int length)
        {
            byte[] data = new byte[length];
            int WriteOffset = 0;

            while (WriteOffset != length && stream.Position != stream.Length)
            {
                int read = stream.Read(data, WriteOffset, data.Length - WriteOffset);

                if (read <= 0)
                    return null;

                WriteOffset += read;
            }

            return data;
        }

        public void Flush()
        {
            Stream.Flush();
        }

        public void Dispose()
        {
            if (Stream != null)
            {
                Stream.Close();
                Stream = null;
                IsDisposed = true;
            }
        }
    }
}