﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ManagedCryptFs.Encryptions
{
    /// <summary>
    /// HwAes (Hardware Accelerated AES) by using the AesCryptoServiceProvider
    /// </summary>
    public sealed class HwAes : IDisposable
    {
        private AesCryptoServiceProvider AES;
        private RNGCryptoServiceProvider rngProvider;

        public DataConfuser IvConfuser { get; set; }

        public byte[] Key
        {
            get { return AES.Key; }
            set { AES.Key = value; }
        }
        public byte[] IV
        {
            get { return AES.IV; }
            set { AES.IV = value; }
        }

        public HwAes(int Seed, byte[] Key, int KeySize, CipherMode cipherMode, PaddingMode padding)
        {
            this.AES = new AesCryptoServiceProvider();
            this.AES.Padding = padding;
            this.AES.Mode = cipherMode;
            this.AES.KeySize = KeySize;
            this.rngProvider = new RNGCryptoServiceProvider();
            ApplyKey(Key);
        }

        public byte[] Encrypt(byte[] Data, int Offset, int Length)
        {
            lock (AES)
            {
                byte[] NewIV = new byte[16];
                rngProvider.GetBytes(NewIV);
                this.IV = NewIV;

                //mask the IV to make it harder to grab the IV while packet sniffing / MITM
                if(IvConfuser != null)
                    IvConfuser.Obfuscate(ref NewIV, 0);

                using (ICryptoTransform Encryptor = AES.CreateEncryptor())
                {
                    using(MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(NewIV, 0, NewIV.Length);

                        byte[] encrypted = Encryptor.TransformFinalBlock(Data, Offset, Length);
                        ms.Write(encrypted, 0, encrypted.Length);
                        return ms.ToArray();
                    }
                }
            }
        }

        public byte[] Decrypt(byte[] Data, int Offset, int Length)
        {
            lock (AES)
            {
                if (Length < 16)
                    return Data;

                //Copy the IV
                byte[] newIV = new byte[16];
                Array.Copy(Data, Offset, newIV, 0, 16);

                if (IvConfuser != null)
                    IvConfuser.Deobfuscate(ref newIV, 0); //unmask the new IV

                this.IV = newIV;
                
                using (ICryptoTransform Decryptor = AES.CreateDecryptor())
                {
                    return Decryptor.TransformFinalBlock(Data, Offset + 16, Length - 16);
                }
            }
        }

        private byte[] KeyExtender(byte[] Input, int TargetLen)
        {
            if (Input.Length == TargetLen)
                return Input;

            int temp = 0xFF28423;
            for (int i = 0; i < Input.Length; i++)
                temp += Input[i];

            int oldLen = Input.Length;
            Random rnd = new Random(temp);
            Array.Resize(ref Input, TargetLen);

            for (int i = oldLen; i < TargetLen; i++)
                Input[i] = (byte)rnd.Next(0, 255);
            return Input;
        }

        public void ApplyKey(byte[] Key)
        {
            this.AES.Key = KeyExtender(Key, 32);
        }

        public void Dispose()
        {
            AES.Clear();
        }
    }
}
