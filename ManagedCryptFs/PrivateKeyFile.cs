﻿using ManagedCryptFs.Encryptions;
using ManagedCryptFs.Security;
using ManagedCryptFs.Utils;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ManagedCryptFs
{
    [ProtoContract]
    public class PrivateKeyFile
    {
        [ProtoMember(1)]
        public string PrivateKey { get; set; }

        [ProtoMember(2)]
        public string PublicKey { get; set; }

        [ProtoMember(3)]
        public List<byte[]> SecretKeys { get; set; }

        public PrivateKeyFile()
        {

        }

        public static PrivateKeyFile GenerateKeyFile()
        {
            SecureRandom rnd = new SecureRandom();

            RSAEncryption GenPrivateKeyc = new RSAEncryption(4096, true);
            GenPrivateKeyc.GeneratePrivateKey();
            GenPrivateKeyc.GeneratePublicKey();
            string PreGenPrivateKey = GenPrivateKeyc.PrivateKey;

            PrivateKeyFile keyFile = new PrivateKeyFile();

            keyFile.PrivateKey = GenPrivateKeyc.PrivateKey;
            keyFile.PublicKey = GenPrivateKeyc.PublicKey;

            keyFile.SecretKeys = new List<byte[]>();

            //add 32 "secret" keys
            for (int i = 0; i < 32; i++)
            {
                keyFile.SecretKeys.Add(rnd.NextBytes(1024));
            }

            return keyFile;
        }

        public void EncryptKeyFile(Stream DestFs, string Password, ushort PIM, Stream[] KeyFiles)
        {
            if (PIM == 0)
                throw new ArgumentException("Invalid PIM", "PIM");

            PBKDF2 passGen = new PBKDF2();
            passGen.CreateHash(Password, PIM);

            string HashedPass = Convert.ToBase64String(passGen.Hash);

            byte[] Key = GenerateKey(HashedPass, KeyFiles, passGen.Salt, PIM);
            
            using (MemoryStream ms = new MemoryStream())
            {
                Serializer.Serialize(ms, this);
                byte[] SerializedKeyFile = ms.ToArray();

                //encrypt the private key file (156 x PIM) times with different keys for TwoFish
                List<byte[]> CipherKeys = GetCipherKeys(passGen.Salt, Key, PIM);
                List<byte[]> CipherSalts = GetCipherSalts(passGen.Salt, Key, PIM);

                int Seed = BitConverter.ToInt32(passGen.Salt, 0);
                int SeedConfuser = BitConverter.ToInt32(passGen.Salt, 4);

                using (HwAes aes = new HwAes(Seed, Key, 256, CipherMode.CBC, PaddingMode.PKCS7))
                {
                    aes.IvConfuser = new DataConfuser(SeedConfuser, 16);

                    for (int i = 0; i < CipherKeys.Count; i++)
                    {
                        byte[] TempKey = CipherKeys[i];
                        byte[] TempSalt = CipherSalts[i];

                        if (TempKey.Length > 32)
                        {
                            //can happen with the very first key being 64bytes
                            Array.Resize(ref TempKey, 32);
                        }

                        TwofishManaged twoFish = new TwofishManaged();
                        ICryptoTransform encryptTransformer = twoFish.CreateEncryptor(TempKey, TempSalt);
                    
                        SerializedKeyFile = aes.Encrypt(SerializedKeyFile, 0, SerializedKeyFile.Length);
                        SerializedKeyFile = encryptTransformer.TransformFinalBlock(SerializedKeyFile, 0, SerializedKeyFile.Length);
                    }
                }

                DestFs.Write(passGen.Salt, 0, passGen.Salt.Length);
                DestFs.Write(SerializedKeyFile, 0, SerializedKeyFile.Length);
                DestFs.Flush();
            }
        }

        public void DecryptKeyFile(Stream FromFs, string Password, ushort PIM, Stream[] KeyFiles)
        {
            if (PIM == 0)
                throw new ArgumentException("Invalid PIM", "PIM");

            byte[] Salt = new byte[PBKDF2.SALT_BYTE_SIZE];
            FromFs.Read(Salt, 0, Salt.Length);

            byte[] SerializedKeyFile = new byte[FromFs.Length - Salt.Length];
            FromFs.Read(SerializedKeyFile, 0, SerializedKeyFile.Length);

            PBKDF2 passGen = new PBKDF2();
            string HashedPass = Convert.ToBase64String(passGen.ComputeHash(Password, Salt, PBKDF2.GetIterations(PIM), PBKDF2.HASH_BYTE_SIZE));
            
            byte[] Key = GenerateKey(HashedPass, KeyFiles, Salt, PIM);

            //encrypt the private key file (156 x PIM) times with different keys for TwoFish
            List<byte[]> CipherKeys = GetCipherKeys(Salt, Key, PIM);
            List<byte[]> CipherSalts = GetCipherSalts(Salt, Key, PIM);

            int Seed = BitConverter.ToInt32(Salt, 0);
            int SeedConfuser = BitConverter.ToInt32(Salt, 4);

            using (HwAes aes = new HwAes(Seed, Key, 256, CipherMode.CBC, PaddingMode.PKCS7))
            {
                aes.IvConfuser = new DataConfuser(SeedConfuser, 16);

                for (int i = CipherKeys.Count - 1; i >= 0; i--)
                {
                    byte[] TempKey = CipherKeys[i];
                    byte[] TempSalt = CipherSalts[i];

                    if (TempKey.Length > 32)
                    {
                        //can happen with the very first key being 64bytes
                        Array.Resize(ref TempKey, 32);
                    }

                    TwofishManaged twoFish = new TwofishManaged();
                    ICryptoTransform decryptTransformer = twoFish.CreateDecryptor(TempKey, TempSalt);
                    
                    SerializedKeyFile = decryptTransformer.TransformFinalBlock(SerializedKeyFile, 0, SerializedKeyFile.Length);
                    SerializedKeyFile = aes.Decrypt(SerializedKeyFile, 0, SerializedKeyFile.Length);
                }
            }

            PrivateKeyFile keyFile = (PrivateKeyFile)Serializer.Deserialize(new MemoryStream(SerializedKeyFile), typeof(PrivateKeyFile));
            this.PublicKey = keyFile.PublicKey;
            this.PrivateKey = keyFile.PrivateKey;
            this.SecretKeys = keyFile.SecretKeys;
        }

        private List<byte[]> GetCipherKeys(byte[] Salt, byte[] Key, ushort PIM)
        {
            List<byte[]> keys = new List<byte[]>();
            byte[] TempKey = new byte[32];
            FastRandom rnd = new FastRandom(BitConverter.ToInt32(Key, 0));

            keys.Add(Key); //use the original PBKDF2 hash first

            for (int i = 0; i < 156 * PIM; i++)
            {
                for (int j = 0; j < (Salt.Length / 2); j++)
                {
                    TempKey[j] += Salt[j];
                    TempKey[j] += Key[j];
                    TempKey[j + (Salt.Length / 2)] += Salt[j + (Salt.Length / 2)];
                    TempKey[j + (Salt.Length / 2)] += Key[j];
                    TempKey[j] += (byte)rnd.Next(0, 255);
                }
                keys.Add(TempKey.ToArray());
            }
            return keys;
        }

        private List<byte[]> GetCipherSalts(byte[] Salt, byte[] Key, ushort PIM)
        {
            List<byte[]> salts = new List<byte[]>();
            byte[] TempIV = new byte[16];
            FastRandom rnd = new FastRandom(BitConverter.ToInt32(Salt, 0));

            rnd.NextBytes(TempIV);
            salts.Add(TempIV);

            for (int i = 0; i < 156 * PIM; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    TempIV[j] += Salt[j];
                    TempIV[j] ^= Key[j];
                    TempIV[j] += (byte)rnd.Next(0, 255);
                }
                salts.Add(TempIV.ToArray());
            }
            return salts;
        }

        private byte[] GenerateKey(string Password, Stream[] KeyFiles, byte[] Salt, ushort PIM)
        {
            SHA512 sha = SHA512.Create();
            byte[] EncPass = sha.ComputeHash(ASCIIEncoding.UTF8.GetBytes(Password));

            List<byte[]> KeyFileData = new List<byte[]>();

            //read only the first 16KB of all the keyfiles
            foreach (Stream stream in KeyFiles)
            {
                stream.Position = 0;

                byte[] AdditionalKey = new byte[16 * 1024];
                int read = stream.Read(AdditionalKey, 0, AdditionalKey.Length);

                if (read > 0)
                {
                    Array.Resize(ref AdditionalKey, read);
                    KeyFileData.Add(AdditionalKey);
                }
            }

            //apply (20,000 x PIM) additional iterations of SHA512 for the Password to make it "slow"
            //apply key file(s)
            //apply Salt
            for (int i = 0; i < 20000 * PIM; i++)
            {
                EncPass = sha.ComputeHash(EncPass);

                HMacLayer hmac = new HMacLayer(EncPass);

                foreach (byte[] keyData in KeyFileData)
                {
                    byte[] hmacData = hmac.ComputeHash(keyData, 0, keyData.Length);

                    for (int x = 0; x < 3; x++)
                    {
                        for (int j = 0; j < hmacData.Length; j++)
                        {
                            EncPass[(x * j) + j & EncPass.Length - 1] += hmacData[j];
                            EncPass[(x * j) + j & EncPass.Length - 1] += Salt[j % Salt.Length];
                        }
                    }

                    for (int j = 0; j < keyData.Length; j++)
                    {
                        EncPass[j % EncPass.Length] += keyData[j];
                        EncPass[j % EncPass.Length] += Salt[j % Salt.Length];
                    }
                }
            }

            return EncPass;
        }

        public byte[] EncryptData(byte[] Data, int offset, int length)
        {
            RSAEncryption rsa = new RSAEncryption(4096, true);
            rsa.LoadPublicKey(PublicKey);
            rsa.LoadPrivateKey(PrivateKey);

            byte[] EncryptedData = rsa.Encrypt(Data, offset, length);

            int Iterations = SecretKeys.Count / 2;
            for (int i = 0; i < Iterations; i++)
            {
                byte[] Key = SecretKeys[i];
                byte[] Salt = SecretKeys[i + Iterations];

                Array.Resize(ref Key, 32);
                Array.Resize(ref Salt, 16);

                int Seed = BitConverter.ToInt32(Salt, 0);

                using (HwAes aes = new HwAes(Seed, Key, 256, CipherMode.CBC, PaddingMode.PKCS7))
                {
                    aes.IvConfuser = new DataConfuser(Seed, 16);

                    TwofishManaged twoFish = new TwofishManaged();
                    ICryptoTransform encryptTransformer = twoFish.CreateEncryptor(Key, Salt);

                    EncryptedData = aes.Encrypt(EncryptedData, 0, EncryptedData.Length);
                    EncryptedData = encryptTransformer.TransformFinalBlock(EncryptedData, 0, EncryptedData.Length);
                }
            }
            return EncryptedData;
        }

        public byte[] DecryptData(byte[] Data, int offset, int length)
        {
            RSAEncryption rsa = new RSAEncryption(4096, true);
            rsa.LoadPublicKey(PublicKey);
            rsa.LoadPrivateKey(PrivateKey);

            int KeyIndex = (SecretKeys.Count / 2) - 1;
            int SaltIndex = SecretKeys.Count - 1;

            int Iterations = SecretKeys.Count / 2;
            for (int i = 0; i < Iterations; i++)
            {
                byte[] Key = SecretKeys[KeyIndex];
                byte[] Salt = SecretKeys[SaltIndex];

                Array.Resize(ref Key, 32);
                Array.Resize(ref Salt, 16);

                int Seed = BitConverter.ToInt32(Salt, 0);

                using (HwAes aes = new HwAes(Seed, Key, 256, CipherMode.CBC, PaddingMode.PKCS7))
                {
                    aes.IvConfuser = new DataConfuser(Seed, 16);

                    TwofishManaged twoFish = new TwofishManaged();
                    ICryptoTransform decryptTransformer = twoFish.CreateDecryptor(Key, Salt);

                    Data = decryptTransformer.TransformFinalBlock(Data, 0, Data.Length);
                    Data = aes.Decrypt(Data, 0, Data.Length);
                }
                KeyIndex--;
                SaltIndex--;
            }

            Data = rsa.Decrypt(Data, 0, Data.Length);

            return Data;
        }
    }
}