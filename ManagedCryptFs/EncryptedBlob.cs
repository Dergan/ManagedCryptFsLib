﻿using ManagedCryptFs.Encryptions;
using ManagedCryptFs.Security;
using ManagedCryptFs.Utils;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ManagedCryptFs
{
    [ProtoContract]
    public class EncryptedBlob
    {
        private byte[] _payload;

        [ProtoMember(1)]
        public int BlobSize { get; private set; }

        [ProtoMember(2)]
        public bool IsEncrypted { get; private set; }

        [ProtoMember(3)]
        public byte[] PayloadIntegrityHash { get; private set; }

        [ProtoMember(4)]
        public long Position { get; set; }

        public FileHeader Header { get; set; }
        internal EncryptedFile File { get; set; }

        public byte[] Payload
        {
            get
            {
                return _payload;
            }
            set
            {
                if (value.Length > EncryptedFile.MAX_PAYLOAD_BLOB_SIZE)
                    throw new Exception("Payload is too big");

                _payload = value;
            }
        }

        public bool IsPayloadDecrypted
        {
            get { return Payload != null; }
        }


        public EncryptedBlob()
        {

        }

        public EncryptedBlob(EncryptedFile File)
        {
            this.File = File;
        }

        public bool IsBlobEncrypted(Stream stream, long Position)
        {
            try
            {
                EncryptedBlob blob = (EncryptedBlob)Serializer.Deserialize(stream, typeof(EncryptedBlob));
                return blob.IsEncrypted;
            }
            catch { }
            return false;
        }

        /// <summary>
        /// Directly write the encrypted data to disk
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="Position"></param>
        public void EncryptBlob(Stream stream, long Position)
        {
            SecureRandom rnd = new SecureRandom();
            byte[] blob = rnd.NextBytes(EncryptedFile.MAX_PAYLOAD_BLOB_SIZE);
            Array.Copy(Payload, blob, Payload.Length);

            //encrypt entire blob here
            int keyIndex = 0;
            foreach(EncryptionType encType in Header.EncryptionTypes)
            {
                switch(encType)
                {
                    case EncryptionType.AES_128:
                    {
                        using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[keyIndex], 128, CipherMode.CBC, PaddingMode.PKCS7))
                        {
                            aes.IvConfuser = Header.IVConfuser;
                            blob = aes.Encrypt(blob, 0, blob.Length);
                        }
                        break;
                    }
                    case EncryptionType.AES_256:
                    {
                        using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[keyIndex], 256, CipherMode.CBC, PaddingMode.PKCS7))
                        {
                            aes.IvConfuser = Header.IVConfuser;
                            blob = aes.Encrypt(blob, 0, blob.Length);
                        }
                        break;
                    }
                }
                keyIndex++;
            }

            this.PayloadIntegrityHash = new HMacLayer(Header.MasterKeys[0]).ComputeHash(blob, 0, blob.Length);
            this.BlobSize = Payload.Length;
            this.IsEncrypted = true;

            using (MemoryStream TempStream = new MemoryStream())
            using (MemoryStream DestFinalStream = new MemoryStream())
            {
                Serializer.Serialize(TempStream, this);

                using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[0], 256, CipherMode.CBC, PaddingMode.PKCS7))
                {
                    aes.IvConfuser = Header.IVConfuser;

                    byte[] EncryptedHeader = aes.Encrypt(TempStream.ToArray(), 0, (int)TempStream.Length);
                    byte[] EncryptedHeaderSize = aes.Encrypt(BitConverter.GetBytes(EncryptedHeader.Length), 0, 4);
                    byte[] EncryptedPayloadSize = aes.Encrypt(BitConverter.GetBytes(blob.Length), 0, 4);

                    DestFinalStream.Write(EncryptedHeaderSize, 0, EncryptedHeaderSize.Length);
                    DestFinalStream.Write(EncryptedHeader, 0, EncryptedHeader.Length);
                    
                    int HeaderReservedSpace = EncryptedFile.BLOB_HEADER_SIZE - EncryptedHeader.Length - EncryptedHeaderSize.Length;

                    if (HeaderReservedSpace > 0)
                    {
                        byte[] HeaderReserved = rnd.NextBytes(HeaderReservedSpace);
                        DestFinalStream.Write(HeaderReserved, 0, HeaderReserved.Length);
                    }

                    DestFinalStream.Write(EncryptedPayloadSize, 0, EncryptedPayloadSize.Length);
                    DestFinalStream.Write(blob, 0, blob.Length);

                    int PayloadReservedSpace = EncryptedFile.PAYLOAD_BLOB_SIZE - blob.Length - EncryptedPayloadSize.Length;

                    if (PayloadReservedSpace > 0)
                    {
                        byte[] PayloadReserved = rnd.NextBytes(PayloadReservedSpace);
                        DestFinalStream.Write(PayloadReserved, 0, PayloadReserved.Length);
                    }

                    stream.Position = Position;
                    stream.Write(DestFinalStream.GetBuffer(), 0, (int)DestFinalStream.Length);
                    stream.Flush();
                }
            }
        }

        public void DecryptBlobHeader(Stream stream, long Position)
        {
            stream.Position = Position;
            using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[0], 256, CipherMode.CBC, PaddingMode.PKCS7))
            {
                aes.IvConfuser = Header.IVConfuser;

                byte[] HeaderSizeData = File.ReadData(stream, 32);

                if (HeaderSizeData == null)
                    throw new Exception("Header corrupt");

                HeaderSizeData = aes.Decrypt(HeaderSizeData, 0, HeaderSizeData.Length);
                int HeaderSize = BitConverter.ToInt32(HeaderSizeData, 0);

                byte[] HeaderData = File.ReadData(stream, HeaderSize);

                if (HeaderData == null)
                    throw new Exception("Header corrupt");

                HeaderData = aes.Decrypt(HeaderData, 0, HeaderData.Length);
                EncryptedBlob blob = (EncryptedBlob)Serializer.Deserialize(new MemoryStream(HeaderData), typeof(EncryptedBlob));

                this.BlobSize = blob.BlobSize;
                this.IsEncrypted = blob.IsEncrypted;
                this.PayloadIntegrityHash = blob.PayloadIntegrityHash;
                this.Position = blob.Position;
            }
        }

        public void DecryptPayload(Stream FromFs)
        {
            if (this.Position == 0)
                throw new Exception("Decrypt header first");

            if (IsPayloadDecrypted)
                return; //no need to decrypt it twice

            long PayloadPos = this.Position + EncryptedFile.BLOB_HEADER_SIZE;
            FromFs.Position = PayloadPos;

            byte[] PayloadSizeData = File.ReadData(FromFs, 32);

            if (PayloadSizeData == null)
                throw new Exception("Header corrupt");

            using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[0], 256, CipherMode.CBC, PaddingMode.PKCS7))
            {
                aes.IvConfuser = Header.IVConfuser;
                PayloadSizeData = aes.Decrypt(PayloadSizeData, 0, PayloadSizeData.Length);
            }

            int PayloadSize = BitConverter.ToInt32(PayloadSizeData, 0);
            byte[] _payload = File.ReadData(FromFs, PayloadSize);
            byte[] hmac = new HMacLayer(Header.MasterKeys[0]).ComputeHash(_payload, 0, _payload.Length);

            if (!new CompareUtils().ConstantTimeEquals(hmac, this.PayloadIntegrityHash))
            {
                throw new Exception("Data Tampered or Corrupt");
            }
            
            for(int i = Header.EncryptionTypes.Count - 1; i >= 0; i--)
            {
                EncryptionType encType = Header.EncryptionTypes[i];

                switch (encType)
                {
                    case EncryptionType.AES_128:
                    {
                        using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[i], 128, CipherMode.CBC, PaddingMode.PKCS7))
                        {
                            aes.IvConfuser = Header.IVConfuser;
                            _payload = aes.Decrypt(_payload, 0, _payload.Length);
                        }
                        break;
                    }
                    case EncryptionType.AES_256:
                    {
                        using (HwAes aes = new HwAes(Header.RandomSeed, Header.MasterKeys[i], 256, CipherMode.CBC, PaddingMode.PKCS7))
                        {
                            aes.IvConfuser = Header.IVConfuser;
                            _payload = aes.Decrypt(_payload, 0, _payload.Length);
                        }
                        break;
                    }
                }
            }

            Array.Resize(ref _payload, this.BlobSize);
            this.Payload = _payload;
        }

        public void CleanCache()
        {
            _payload = null;
        }

        public override string ToString()
        {
            return String.Format("[Blob] BlobSize: {0}, Position: {1}", BlobSize, Position);
        }
    }
}