﻿using ManagedCryptFs.Encryptions;
using ManagedCryptFs.Security;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ManagedCryptFs
{
    [ProtoContract]
    public class FileHeader
    {
        private int randSeed = 0;

        //The file header currently has room for <= 4KB reserved space incase of updates
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public int RandomSeed
        {
            get { return randSeed; }
            set
            {
                randSeed = value;
                IVConfuser = new DataConfuser(value, 16);
            }
        }

        [ProtoMember(3)]
        public List<byte[]> MasterKeys { get; set; }

        [ProtoMember(4)]
        public List<EncryptionType> EncryptionTypes { get; private set; }

        [ProtoMember(5)]
        public byte[] Salt { get; set; }

        public DataConfuser IVConfuser { get; private set; }

        internal EncryptedFile File { get; set; }

        public FileHeader()
        {
            this.EncryptionTypes = new List<EncryptionType>();
            this.MasterKeys = new List<byte[]>();
        }

        public void EncryptHeader(Stream DestFs, long Position, PrivateKeyFile keyFile)
        {
            using (MemoryStream TempStream = new MemoryStream())
            using (MemoryStream DestMemStream = new MemoryStream())
            {
                //encrypt header using RSA
                Serializer.Serialize(TempStream, this);
                byte[] EncryptedHeader = keyFile.EncryptData(TempStream.ToArray(), 0, (int)TempStream.Length);

                int ReservedSize = EncryptedFile.FILE_HEADER_SIZE - EncryptedHeader.Length - 1280; //EncryptedHeaderSize=1280 length
                byte[] EncryptedHeaderSize = keyFile.EncryptData(BitConverter.GetBytes(EncryptedHeader.Length), 0, 4);

                DestMemStream.Write(EncryptedHeaderSize, 0, EncryptedHeaderSize.Length);
                DestMemStream.Write(EncryptedHeader, 0, EncryptedHeader.Length);

                if (ReservedSize > 0)
                {
                    SecureRandom rand = new SecureRandom();
                    byte[] reserved = rand.NextBytes(ReservedSize);
                    DestMemStream.Write(reserved, 0, reserved.Length);
                }
                DestFs.Write(DestMemStream.ToArray(), 0, (int)DestMemStream.Length);
                DestFs.Flush();
            }
        }

        public void DecryptHeader(Stream FromFs, long Position, PrivateKeyFile keyFile)
        {
            FromFs.Position = 0;
            byte[] HeaderSizeBytes = File.ReadData(FromFs, 1280);

            if (HeaderSizeBytes == null)
                throw new Exception("Invalid file");

            HeaderSizeBytes = keyFile.DecryptData(HeaderSizeBytes, 0, HeaderSizeBytes.Length);
            int HeaderSize =  BitConverter.ToInt32(HeaderSizeBytes, 0);

            byte[] HeaderData = File.ReadData(FromFs, HeaderSize);
            HeaderData = keyFile.DecryptData(HeaderData, 0, HeaderData.Length);

            FileHeader header = (FileHeader)Serializer.Deserialize(new MemoryStream(HeaderData), typeof(FileHeader));
            this.Name = header.Name;
            this.EncryptionTypes = header.EncryptionTypes;
            this.MasterKeys = header.MasterKeys;
            this.RandomSeed = header.RandomSeed;
            this.Salt = header.Salt;
        }
    }
}