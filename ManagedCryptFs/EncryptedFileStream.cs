﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ManagedCryptFs
{
    public class EncryptedFileStream : Stream
    {
        private const int EXTRA_BLOB_SPACE = 0;

        private object IOLock = new object();
        public EncryptedFile File { get; private set; }

        public EncryptedFileStream(EncryptedFile File)
            : base()
        {
            this.File = File;
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override long Length
        {
            get
            {
                lock (IOLock)
                {
                    if (File.BlobCount == 0)
                        return 0;

                    long _length = 0;

                    long LastBlobIndex = File.BlobCount - 1 - EXTRA_BLOB_SPACE;
                    EncryptedBlob LastBlob = File.GetBlobByIndex(LastBlobIndex);

                    if (LastBlobIndex > 0)
                    {
                        _length = LastBlobIndex * EncryptedFile.MAX_PAYLOAD_BLOB_SIZE;
                    }

                    return LastBlob.BlobSize + _length;
                }
            }
        }

        public override long Position
        {
            get;
            set;
        }

        public override void Flush()
        {
            File.Stream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            lock (IOLock)
            {
                RwOffset[] offsets = GetRwOffsets(Position, count, false);

                int TotalRead = 0;
                int writeOffset = offset;

                foreach (RwOffset rwOffset in offsets)
                {
                    rwOffset.Blob.DecryptPayload(File.Stream);

                    int readLen = rwOffset.RwSize;
                    if (rwOffset.BlobPosition + rwOffset.RwSize > rwOffset.Blob.BlobSize)
                    {
                        readLen = rwOffset.Blob.BlobSize - rwOffset.BlobPosition;
                    }

                    Array.Copy(rwOffset.Blob.Payload, rwOffset.BlobPosition, buffer, writeOffset, readLen);
                    writeOffset += readLen;
                    TotalRead += readLen;
                }

                Position += TotalRead;
                return TotalRead;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            lock (IOLock)
            {
                PrepareReadWrite(count);
                
                RwOffset[] offsets = GetRwOffsets(Position, count, true);

                int readOffset = offset;
                foreach (RwOffset rwOffset in offsets)
                {
                    rwOffset.Blob.DecryptPayload(File.Stream);
                    
                    int Len = rwOffset.BlobPosition + rwOffset.RwSize;
                    if (rwOffset.Blob.Payload.Length < Len)
                    {
                        byte[] TempPayload = rwOffset.Blob.Payload;
                        Array.Resize(ref TempPayload, Len);
                        rwOffset.Blob.Payload = TempPayload;
                    }
                    
                    Array.Copy(buffer, readOffset, rwOffset.Blob.Payload, rwOffset.BlobPosition, rwOffset.RwSize);
                    
                    readOffset += rwOffset.RwSize;
                    rwOffset.Blob.EncryptBlob(File.Stream, rwOffset.Blob.Position);
                }
                Position += count;
            }
        }

        public int GetBlobIndex(long Position)
        {
            return (int)(Position / EncryptedFile.MAX_PAYLOAD_BLOB_SIZE);
        }

        private void PrepareReadWrite(int ReadCount)
        {
            int blobIndex = GetBlobIndex(this.Position);
            int blobEndIndex = GetBlobIndex(this.Position + ReadCount);

            //pre-create all the blobs that we need
            //prepare extra space aswell as "padding"
            while (File.BlobCount <= blobEndIndex + EXTRA_BLOB_SPACE)
            {
                EncryptedBlob blob = new EncryptedBlob(this.File);
                blob.Header = File.Header;
                blob.Payload = new byte[0];
                blob.Position = (File.BlobCount * EncryptedFile.BLOB_SIZE) + EncryptedFile.FILE_HEADER_SIZE;
                blob.EncryptBlob(File.Stream, blob.Position);
                blob.CleanCache();
            }
        }


        private RwOffset[] GetRwOffsets(long Position, int ReadSize, bool IsWrite)
        {
            List<RwOffset> offsets = new List<RwOffset>();
            
            long PrevBlockSizes = 0;
            List<EncryptedBlob> blobs = new List<EncryptedBlob>();

            long StartBlobIndex = File.GetIndexByPosition(Position);
            long EndBlobIndex = File.GetIndexByPosition(Position + ReadSize);

            if (StartBlobIndex > 0)
            {
                PrevBlockSizes = StartBlobIndex * EncryptedFile.MAX_PAYLOAD_BLOB_SIZE;
            }

            for (long i = StartBlobIndex; i <= EndBlobIndex; i++)
            {
                EncryptedBlob blob = File.GetBlobByIndex(i);

                if (blob == null)
                    break;

                blobs.Add(blob);
            }

            foreach (EncryptedBlob blob in blobs)
            {
                while (Position < (PrevBlockSizes + EncryptedFile.MAX_PAYLOAD_BLOB_SIZE) && ReadSize > 0)
                {
                    int size = ReadSize > EncryptedFile.MAX_PAYLOAD_BLOB_SIZE ? EncryptedFile.MAX_PAYLOAD_BLOB_SIZE : ReadSize;
                    int offset = (int)(Position - PrevBlockSizes);

                    if (IsWrite && offset + size > EncryptedFile.MAX_PAYLOAD_BLOB_SIZE)
                    {
                        size = EncryptedFile.MAX_PAYLOAD_BLOB_SIZE - offset;
                    }
                    else if (!IsWrite && offset + size > blob.BlobSize)
                    {
                        if (blob.BlobSize == 0)
                            return offsets.ToArray();

                        size = EncryptedFile.MAX_PAYLOAD_BLOB_SIZE - offset;
                    }

                    offsets.Add(new RwOffset(blob, offset, size));

                    ReadSize -= size;
                    Position += size;
                }
                PrevBlockSizes += EncryptedFile.MAX_PAYLOAD_BLOB_SIZE;

                if (ReadSize == 0)
                    break;
            }

            return offsets.ToArray();
        }

        private class RwOffset
        {
            public EncryptedBlob Blob { get; private set; }
            public int BlobPosition { get; private set; }
            public int RwSize { get; private set; }

            public RwOffset(EncryptedBlob Blob, int BlobPosition, int RwSize)
            {
                this.Blob = Blob;
                this.BlobPosition = BlobPosition;
                this.RwSize = RwSize;
            }
        }
    }
}