﻿using ManagedCryptFs.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


//Full credits go to https://codereview.stackexchange.com/questions/32856/secure-password-hashing
//Small changes were made by AnguisCaptor
namespace ManagedCryptFs.Security
{
    public class PBKDF2
    {
        // The following constants may be changed without breaking existing hashes.
        public const int SALT_BYTE_SIZE = 16;
        public const int HASH_BYTE_SIZE = 20;
        public const int PBKDF2_ITERATIONS = 1081789;
        public const int PIM_ITERATIONS = 12037;

        public const int ITERATION_INDEX = 0;
        public const int SALT_INDEX = 1;
        public const int PBKDF2_INDEX = 2;

        public byte[] Salt { get; private set; }
        public byte[] Hash { get; private set; }

        /// <summary>
        /// Creates a salted PBKDF2 hash of the password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>The hash of the password.</returns>
        public string CreateHash(string password, ushort PIM)
        {
            this.Salt = new byte[SALT_BYTE_SIZE];

            // Generate a random salt
            using (RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider())
            {
                csprng.GetBytes(this.Salt);
            }

            // Hash the password and encode the parameters
            this.Hash = ComputeHash(password, this.Salt, GetIterations(PIM), HASH_BYTE_SIZE);
            return PBKDF2_ITERATIONS + ":" + Convert.ToBase64String(this.Salt) + ":" + Convert.ToBase64String(this.Hash);
        }

        /// <summary>
        /// Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="correctHash">A hash of the correct password.</param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        public bool ValidatePassword(string password, string correctHash)
        {
            // Extract the parameters from the hash
            char[] delimiter = { ':' };
            string[] split = correctHash.Split(delimiter);
            int iterations = Int32.Parse(split[ITERATION_INDEX]);
            byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);
            byte[] hash = Convert.FromBase64String(split[PBKDF2_INDEX]);

            byte[] testHash = ComputeHash(password, salt, iterations, hash.Length);
            return new CompareUtils().ConstantTimeEquals(hash, testHash);
        }

        /// <summary>
        /// Computes the PBKDF2-SHA1 hash of a password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="iterations">The PBKDF2 iteration count.</param>
        /// <param name="outputBytes">The length of the hash to generate, in bytes.</param>
        /// <returns>A hash of the password.</returns>
        public byte[] ComputeHash(string password, byte[] salt, int iterations, int outputBytes)
        {
            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt))
            {
                pbkdf2.IterationCount = iterations;
                return pbkdf2.GetBytes(outputBytes);
            }
        }

        public static int GetIterations(int PIM)
        {
            return PBKDF2_ITERATIONS + (PIM_ITERATIONS * PIM);
        }
    }
}
