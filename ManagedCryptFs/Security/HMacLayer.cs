﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ManagedCryptFs.Security
{
    public class HMacLayer
    {
        private HMAC hMac;

        public HMacLayer(HMAC hMac)
        {
            this.hMac = hMac;
        }

        public HMacLayer(byte[] Key)
        {
            hMac = new HMACSHA1(Key);
        }

        public byte[] ComputeHash(byte[] Data, int Offset, int Length)
        {
            lock (hMac)
            {
                return hMac.ComputeHash(Data, Offset, Length);
            }
        }

        public bool Verify(byte[] DataIntegrityLayerData, byte[] Data, int Offset, int Length)
        {
            lock (hMac)
            {
                if (DataIntegrityLayerData == null || (DataIntegrityLayerData != null && DataIntegrityLayerData.Length < FixedLength))
                    return false;

                byte[] ComputedHash = hMac.ComputeHash(Data, Offset, Length);

                for (int i = 0; i < ComputedHash.Length; i++)
                {
                    if (ComputedHash[i] != DataIntegrityLayerData[i])
                        return false;
                }
                return true;
            }
        }

        public int FixedLength
        {
            get { return hMac.HashSize / 8; } //  divide by 8 to get byte length
        }

        public void ApplyKey(byte[] Key, byte[] Salt, int Seed)
        {
            lock (hMac)
            {
                Random rnd = new Random(Seed);

                byte[] VerifyKey = new byte[32];
                rnd.NextBytes(VerifyKey);

                for (int i = 0; i < Key.Length; i++)
                {
                    VerifyKey[i % (VerifyKey.Length - 1)] += Key[i];
                    VerifyKey[i % (VerifyKey.Length - 1)] += Salt[i % (Salt.Length - 1)];
                }

                hMac.Key = VerifyKey;
            }
        }
    }
}
