﻿using ManagedCryptFs;
using ManagedCryptFs.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            string OrgPath = "./TestFiles/testfile.jpg";
            string EncryptedPath = "./TestFiles/testfile.jpg.mfs";
            string DecryptedPath = "./TestFiles/testfile_decrypted.jpg";
            string BigTestFilePath = "./TestFiles/bigtestfile.dat";

            string PrivateKeyFilePath = "./TestFiles/PrivateKeyFile.dat";
            string TestKeyFilePath = "./TestFiles/TempKeyFile.dat";

            string TestDevFilePath = "./TestFiles/TestDevFile.dat";


            //remove 
            if (File.Exists(EncryptedPath))
                File.Delete(EncryptedPath);

            if (File.Exists(DecryptedPath))
                File.Delete(DecryptedPath);

            if (File.Exists(BigTestFilePath))
                File.Delete(BigTestFilePath);


            Console.WriteLine("Generating/Opening the key file");
            Stopwatch keyFileSw = Stopwatch.StartNew();
            //generate a new key file to decrypt/encrypt key (RSA-4096)
            PrivateKeyFile keyFile = new PrivateKeyFile();
            keyFile = PrivateKeyFile.GenerateKeyFile();

            if (!File.Exists(PrivateKeyFilePath))
            {
                //encrypt the key to secure it to disk (sample)
                //save the new key file if it did not exist
                Console.WriteLine("Generating new key file...");
                keyFile.EncryptKeyFile(new FileStream(PrivateKeyFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite), //destination
                                        "e54756e7h7h4475h", //Password
                                        1,  //PIM
                                        new Stream[]
                                        {
                                            //key file(s)
                                            new FileStream(TestKeyFilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
                                        });
            }



            Console.WriteLine("Decrypting key file...");
            keyFile.DecryptKeyFile(new FileStream(PrivateKeyFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite), //source
                                        "e54756e7h7h4475h", //Password
                                        1,  //PIM
                                        new Stream[]
                                        {
                                            //key file(s)
                                            new FileStream(TestKeyFilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
                                        });


            keyFileSw.Stop();
            Console.WriteLine("Took " + keyFileSw.Elapsed.TotalSeconds + "sec to open or generate the key file");




            //create the encrypted file and encrypt it with the data from "testfile.jpg"
            Stopwatch sw = Stopwatch.StartNew();
            using (FileStream FromFs = new FileStream(OrgPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            using (FileStream DestFs = new FileStream(EncryptedPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                EncryptedFile.EncryptFile(FromFs, DestFs, new FileInfo(OrgPath).Name, keyFile);
            }

            sw.Stop();
            Console.WriteLine("Took " + sw.ElapsedMilliseconds + "msec to encrypt the file");



            //add a test file which should only be created once
            //once created, throughout development this file should at all times be usable
            if (!File.Exists(TestDevFilePath))
            {
                using (FileStream DestFs = new FileStream(TestDevFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    using (EncryptedFile encFile = EncryptedFile.EncryptFile(new MemoryStream(), DestFs, new FileInfo(OrgPath).Name, keyFile))
                    using (EncryptedFileStream encStream = new EncryptedFileStream(encFile))
                    {
                        byte[] buffer = new byte[100000];

                        for (int i = 0; i < buffer.Length; i++)
                            buffer[i] = (byte)(i % 100);

                        encStream.Write(buffer, 0, buffer.Length);
                    }
                }
            }
            else
            {
                using (EncryptedFile encFile = EncryptedFile.OpenFile(new FileStream(TestDevFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite), keyFile, 0))
                using (EncryptedFileStream encStream = new EncryptedFileStream(encFile))
                {
                    byte[] buffer = new byte[100000];
                    int read = encStream.Read(buffer, 0, buffer.Length);

                    if (read != buffer.Length)
                        throw new Exception("Invalid read output");

                    for (int i = 0; i < buffer.Length; i++)
                    {
                        if (buffer[i] != i % 100)
                        {
                            throw new Exception("Invalid data read output");
                        }
                    }
                }
            }


            //open the encrypted file

            using (EncryptedFile encFile = EncryptedFile.OpenFile(new FileStream(EncryptedPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite), keyFile, 0))
            {
                //my testing to see if the decrypted file is 100% the same compared to the original
                //we're doing here random read sizes which "might" fail the test
                using (FileStream FromFs = new FileStream(OrgPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    EncryptedFileStream encStream = new EncryptedFileStream(encFile);

                    Random rnd = new Random();
                    
                    for (long i = 0; i < FromFs.Length;)
                    {
                        //read a random length to test stability
                        int ReadLength = 65535;//rnd.Next(1, 1000000);
                        byte[] OrgData = new byte[ReadLength];
                        byte[] EncData = new byte[ReadLength];

                        FromFs.Position = i;
                        encStream.Position = i;

                        //read from original file and encrypted file and compare if it's equal
                        int ReadOrg = FromFs.Read(OrgData, 0, OrgData.Length);
                        int ReadEnc = encStream.Read(EncData, 0, EncData.Length);

                        if (ReadOrg != ReadEnc)
                            throw new Exception("somethings wrong");

                        for (int j = 0; j < ReadLength; j++)
                        {
                            if (OrgData[j] != EncData[j])
                            {
                                throw new Exception("somethings wrong");
                            }
                        }
                        i += ReadLength;
                    }
                }




                //decrypt the file and save it to "testfile_decrypted.jpg"
                Stopwatch decSW = Stopwatch.StartNew();
                using (FileStream stream = new FileStream(DecryptedPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    for(int i = 0; i < encFile.BlobCount; i++)
                    {
                        EncryptedBlob blob = encFile.GetBlobByIndex(i);
                        blob.DecryptPayload(encFile.Stream);
                        stream.Write(blob.Payload, 0, blob.Payload.Length);
                    }
                    stream.Flush();
                }
                decSW.Stop();
                Console.WriteLine("Done Encrypting & Decrypting, took " + decSW.ElapsedMilliseconds + "msec");
            }


            Console.WriteLine();
            Console.WriteLine("Starting in memory encryption/decrypt speed test");
            using (EncryptedFile encFile = EncryptedFile.EncryptFile(new MemoryStream(), new MemoryStream(), new FileInfo(OrgPath).Name, keyFile))
            using (EncryptedFileStream encStream = new EncryptedFileStream(encFile))
            {
                Stopwatch InMemorySw = Stopwatch.StartNew();
                long CurSpeed = 0;
                int Buffersize = 1000000; //1MB buffer
                byte[] Buffer = new byte[Buffersize];
                int IterationsDone = 0;

                while (true)
                {
                    encStream.Position = 0;

                    Stopwatch kek = Stopwatch.StartNew();
                    encStream.Write(Buffer, 0, Buffer.Length);
                    kek.Stop();
                    CurSpeed += Buffer.Length;

                    if (InMemorySw.ElapsedMilliseconds >= 1000)
                    {
                        Console.WriteLine("In Memory Encryption speed: " + Math.Round(((CurSpeed / 1024D) / 1024D), 2) + "MBps");
                        CurSpeed = 0;
                        InMemorySw.Restart();
                        IterationsDone++;

                        if (IterationsDone == 10)
                        {
                            break;
                        }
                    }
                }

                IterationsDone = 0;
                while (true)
                {
                    encStream.Position = 0;
                    int read = encStream.Read(Buffer, 0, Buffer.Length);
                    CurSpeed += Buffer.Length;

                    if (InMemorySw.ElapsedMilliseconds >= 1000)
                    {
                        Console.WriteLine("In Memory Decryption speed: " + Math.Round(((CurSpeed / 1024D) / 1024D), 2) + "MBps");
                        CurSpeed = 0;
                        InMemorySw.Restart();
                        IterationsDone++;

                        if (IterationsDone == 10)
                        {
                            break;
                        }
                    }
                }
            }


            Console.WriteLine();
            Console.WriteLine("Starting stress test, this will run for a little while...");
            Console.WriteLine("#1 - 50MB - Random Buffer Size - 1 Minute run");
            Console.WriteLine("#2 - 50MB - 65KB Buffer - 1 Minute run");
            Console.WriteLine("#3 - 50MB - 4Kib - 5 Minute run");

            Console.WriteLine();
            Console.WriteLine("Starting #1 - 50MB - Random Buffer Size - 1 Minute run");
            using (EncryptedFile encFile = EncryptedFile.EncryptFile(new MemoryStream(), new FileStream(BigTestFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite), "bigtestfile.dat", keyFile))
            using (EncryptedFileStream stream = new EncryptedFileStream(encFile))
            {
                SecureRandom rand = new SecureRandom();
                Stopwatch TotalRunSW = Stopwatch.StartNew();
                Stopwatch RunSW = Stopwatch.StartNew();
                long TotalWritten = 0;
                long WriteSpeed = 0;
                int IterationsDone = 0;

                //write here to the end already to allocate the space
                stream.Position = 50000000; //50MB
                stream.WriteByte(0);

                while (TotalRunSW.Elapsed.Minutes != 1)
                {
                    byte[] buffer = rand.NextBytes(rand.Next(1, 50000000)); //buffer can go upto 50MB

                    stream.Position = rand.Next(0, 50000000); //50MB
                    stream.Write(buffer, 0, buffer.Length);
                    TotalWritten += buffer.Length;
                    WriteSpeed += buffer.Length;

                    if (RunSW.ElapsedMilliseconds >= 1000)
                    {
                        Console.WriteLine("Written Length: " + TotalWritten + ", Current Stream Position: " + stream.Position + ", Speed: " + Math.Round((WriteSpeed / 1000D / 1000D), 2) + "MBps");
                        WriteSpeed = 0;
                        RunSW = Stopwatch.StartNew();
                    }
                }

                TotalRunSW = Stopwatch.StartNew();
                Console.WriteLine();
                Console.WriteLine("Starting #2 - 50MB - 65KB Buffer - 1 Minute run");
                stream.Position = 0;

                while (TotalRunSW.Elapsed.Minutes != 1)
                {
                    byte[] buffer = rand.NextBytes(65535);

                    stream.Write(buffer, 0, buffer.Length);
                    TotalWritten += buffer.Length;
                    WriteSpeed += buffer.Length;

                    if (stream.Position > 50000000)
                    {
                        stream.Position = 0;
                        IterationsDone++;
                    }

                    if (RunSW.ElapsedMilliseconds >= 1000)
                    {
                        Console.WriteLine("Iterations Completed: " + IterationsDone + ", Cur Stream Pos: " + stream.Position + ", Speed: " + Math.Round((WriteSpeed / 1000D / 1000D), 2) + "MBps");
                        WriteSpeed = 0;
                        RunSW = Stopwatch.StartNew();
                    }
                }

                TotalRunSW = Stopwatch.StartNew();
                Console.WriteLine();
                Console.WriteLine("#3 - 50MB - 4Kib - 5 Minute run");
                stream.Position = 0;
                IterationsDone = 0;

                while (TotalRunSW.Elapsed.Minutes != 5)
                {
                    byte[] buffer = rand.NextBytes(1024 * 4);

                    stream.Write(buffer, 0, buffer.Length);
                    TotalWritten += buffer.Length;
                    WriteSpeed += buffer.Length;

                    if (stream.Position > 50000000)
                    {
                        stream.Position = 0;
                        IterationsDone++;
                    }

                    if (RunSW.ElapsedMilliseconds >= 1000)
                    {
                        Console.WriteLine("Iterations Completed: " + IterationsDone + ", Cur Stream Pos: " + stream.Position + ", Speed: " + Math.Round((WriteSpeed / 1000D / 1000D), 2) + "MBps");
                        WriteSpeed = 0;
                        RunSW = Stopwatch.StartNew();
                    }
                }
            }

            Console.WriteLine("Done...");
            Console.ReadLine();
        }
    }
}